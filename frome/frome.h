/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __FROME_H__

#include "dbus/Frome.AccountMgr.h"
#include "dbus/Frome.DnldMgr.h"
#include "dbus/org.apertis.Frome.h"

#define __FROME_H__

#endif /* __FROME_H__ */
