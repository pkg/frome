#!/usr/bin/env python3

# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import http.server
import os
import os.path
import socketserver
import socket
import sys

from gi.repository import Gio, GLib

"""
Mock server emulating the Web Store POST API.

It exposes a simple D-Bus API so tests can know when it's ready
to handle HTTP requests, preventing races, and on which port.

Take the directory containing app bundles as first argument.
Take the port as optional second argument for easier testing.
"""

# Need to stay synced with CUSTOMER_ID in tests/test-service.c
CUSTOMER_ID = 'kHczotvae21ZuY6wkjkQ1jmljCMIioTihcpcTZfLjGLy0Rh2RrD9TSY0rhWSKy6GEXQfY7FntCHaElSzZaveROzLz3V1BGRlH_FKOcLFODaReGfdHnpLyJHEGZiTpDGSbismekMKxMINzaDWeI8J4EjHxmycWiBIxpGKBH-i5GIPaqJygJLMzxEOQqAkHC0ns8YTQvMEupLGxjHsaA5p-Mq97PwRmhD70QTyhQtV0naGXawZ1JCzvrl7AUiRR_lGd_OdzQW8dYWQAEjLF46mlbWbvyfSX0ohSXMo31hdc2lw2btDH1Aha6M4VNzDuOMPJI5gM5Rl2s1LjMg-oxG6uw,,'

class MyTCPServer(socketserver.TCPServer):

    def server_bind(self):
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(self.server_address)


class Handler(http.server.SimpleHTTPRequestHandler):

    def do_POST(self):
        body_len = int(self.headers.get('Content-Length'))
        body = self.rfile.read(body_len).decode()

        # Build a dict from POST arguments
        args = body.split('&')
        args = dict(map(lambda x: x.split('='), args))

        # Each request should have a 'function' argument implemented by a _handle_* method.
        # The '.' in function name is replaced by a '_' to have a valid method
        # name.
        try:
            fct = getattr(self, '_handle_%s' %
                          args['function'].replace('.', '_'))
        except (AttributeError, KeyError):
            self._send_reply(400, "function not found")
            return

        code, msg, xml = fct(args)
        self._send_reply(code, msg, xml)

    def _send_reply(self, code, msg, xml=False):
        self.send_response(code)
        # The mime-type should be 'application/xml' when @xms is True but the
        # actual server always pretends we receive HTML so we do as well.
        self.send_header('Content-type', 'text/html; charset=UTF-8')
        self.end_headers()
        if xml:
            msg = '<?xml version="1.0" encoding="utf-8" standalone="yes"?>\n%s' % (
                msg)
        self.wfile.write(bytes(msg, "utf-8"))

    def _handle_asales_getVersion(self, args):
        return (200, '<version>Adit_Asales_Model_Api Version 1.02 c2012</version>', True)

    def _check_args(self, args, expected_fields):
        for f in expected_fields:
            try:
                if len(args[f]) == 0:
                    print("Missing arg %s" % f)
                    return False
            except KeyError:
                print("Missing arg %s" % f)
                return False

        return True

    def _handle_asales_registerDevice(self, args):
        if not self._check_args(args, ["firstname", "lastname", "e_mail", "password", "order_newsletter", "telephone", "streetname_housenumber", "city", "postalcode", "state", "country"]):
            return (400, 'Bad Request', False)

        # Special case to test failing code path
        if args['e_mail'].startswith('FAIL'):
            return (400, 'Bad Request', False)

        reply = "<customer><customer_id>{0}</customer_id></customer>".format(CUSTOMER_ID)
        return (201, reply, True)

    def _handle_asales_setTargetId(self, args):
        return (201, 'TargetId set for this device', False)

    def _handle_asales_setTargetAppState(self, args):
        return (201, 'TargetAppstate set for this device', False)

    def _handle_asales_setStatus(self, args):
        return (200, '<status>%s</status>' % args['status'], True)

introspection_xml =  \
    '''
    <node>
        <interface name='org.apertis.Frome.MockWebStoreServer'>
          <property type='q' name='Port' access='read'/>
        </interface>
    </node>
    '''


class MockWebStoreServer(object):

    def __init__(self, port=0):
        self.httpd = MyTCPServer(("127.0.0.1", port), Handler)
        self.httpd.timeout = 0.1

        ip, self.port = self.httpd.socket.getsockname()
        print("listening on port", self.port)

    def run(self):
        owner_id = Gio.bus_own_name(Gio.BusType.SESSION,
                                    "org.apertis.Frome.MockWebStoreServer",
                                    Gio.BusNameOwnerFlags.NONE,
                                    self._on_bus_acquired,
                                    self._on_name_acquired,
                                    self._on_name_lost
                                    )

        # Hook the HTTP server with the main loop
        GLib.timeout_add(0.5, self._handle_http)

        self.loop = GLib.MainLoop()

        try:
            self.loop.run()
        except KeyboardInterrupt:
            pass

        Gio.bus_unown_name(owner_id)

    def _handle_http(self):
        self.httpd.handle_request()
        return True

    def _on_bus_acquired(self, connection, name, *args):
        introspection_data = Gio.DBusNodeInfo.new_for_xml(introspection_xml)

        reg_id = connection.register_object(
            "/org/apertis/Frome/MockWebStoreServer",
            introspection_data.interfaces[0],
            None,
            self._handle_get_property,
            None)

        if reg_id == 0:
            print('Error while registering object!')
            self.loop.quit()

    def _on_name_acquired(self, connection, name, *args):
        pass

    def _on_name_lost(self, connection, name, *args):
        self.loop.quit()

    def _handle_get_property(self, connection, sender, object_path, interface, value):
        if value == 'Port':
            return GLib.Variant("q", self.port)

        print ("Unsupported property %s" % value)
        self.loop.quit()

def usage():
    print ("%s BUNDLE_DIRECTORY [PORT]" % sys.argv[0])
    sys.exit(1)

if __name__ == '__main__':
    if len(sys.argv) == 2:
        port = 0
    elif len(sys.argv) == 3:
        port = int(sys.argv[2])
    else:
        usage()

    path = sys.argv[1]
    if not os.path.exists(path):
        usage()

    # Change dir to bundle directory so the web server will be able to serve
    # the bundle files
    os.chdir(path)

    server = MockWebStoreServer(port)
    server.run()
