/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <string.h>

#include <gio/gio.h>

#include "src/xml-parser.h"

typedef struct
{
  const gchar *name;
  const gchar *xml;
} TestScenario;

/* clang-format off */
TestScenario customer_success_tests[] = {
  { "simple", "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><customer><customer_id>my-customer-id</customer_id></customer>" },
  { "nested", "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><foo><bar><customer><customer_id>my-customer-id</customer_id><badger>mushroom</badger></customer></bar></foo>" },
};
/* clang-format off */

static void
test_customer_success (gconstpointer data)
{
  g_autofree gchar *customer_id = NULL;
  gboolean result;
  g_autoptr (GError) error = NULL;

  result = frome_xml_parser_parse_customer (data, strlen (data), &customer_id, &error);
  g_assert_no_error (error);
  g_assert_true (result);
  g_assert_cmpstr (customer_id, ==, "my-customer-id");
}

static void
test_customer_fail (gconstpointer data)
{
  g_autofree gchar *customer_id = NULL;
  gboolean result;
  g_autoptr (GError) error = NULL;

  result = frome_xml_parser_parse_customer (data, strlen (data), &customer_id, &error);
  g_assert_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE);
  g_assert_false (result);
  g_assert (customer_id == NULL);
}

/* clang-format off */
TestScenario customer_fail_tests[] = {
  { "no-customer", "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><customer_id>my-customer-id</customer_id>" },
  { "no-customer-id", "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><customer></customer>" },
  { "empty-customer-id", "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><customer><customer_id></customer_id></customer>" },
  { "two-customer-id", "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><customer><customer_id>my-customer-id</customer_id><customer_id>another-customer-id</customer_id></customer>" },
};
/* clang-format off */

typedef struct
{
  const gchar *name;
  const gchar *xml;
  AppState status;
} StatusTestScenario;

/* clang-format off */
StatusTestScenario status_success_tests[] = {
  { "initial", "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><status>1</status>", APP_STATE_INITIAL },
  { "purchased", "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><status>2</status>", APP_STATE_PURCHASED },
  { "ready", "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><status>3</status>", APP_STATE_DOWNLOAD_READY },
  { "downloading", "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><status>4</status>", APP_STATE_DOWNLOADING },
  { "installing", "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><status>5</status>", APP_STATE_INSTALLING },
  { "installed", "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><status>6</status>", APP_STATE_INSTALLED },
  { "updateable", "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><status>7</status>", APP_STATE_UPDATE },
  { "uninstalled", "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><status>8</status>", APP_STATE_UNINSTALLED },
};
/* clang-format off */

static void
test_status_success (gconstpointer data)
{
  StatusTestScenario *test = (StatusTestScenario *) data;
  gboolean result;
  g_autoptr (GError) error = NULL;
  AppState status;

  result = frome_xml_parser_parse_status (test->xml, strlen (test->xml), &status, &error);
  g_assert_no_error (error);
  g_assert_true (result);
  g_assert_cmpuint (status, ==, test->status);
}

/* clang-format off */
TestScenario status_fail_tests[] = {
  { "no-status", "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><badger>3</badger>" },
  { "invalid", "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><status>666</status>" },
};
/* clang-format off */

static void
test_status_fail (gconstpointer data)
{
  gboolean result;
  g_autoptr (GError) error = NULL;
  AppState status = 0;

  result = frome_xml_parser_parse_status (data, strlen (data), &status, &error);
  g_assert_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE);
  g_assert_false (result);
  g_assert_cmpuint (status, ==, 0);
}

int
main (int argc, char **argv)
{
  guint i;

  g_test_init (&argc, &argv, NULL);

  for (i = 0; i < G_N_ELEMENTS (customer_success_tests); i++)
    {
      g_autofree gchar *path = NULL;

      path = g_strdup_printf ("/frome/xml-parser/customer/sucess/%s", customer_success_tests[i].name);
      g_test_add_data_func (path, customer_success_tests[i].xml, test_customer_success);
    }

  for (i = 0; i < G_N_ELEMENTS (customer_fail_tests); i++)
    {
      g_autofree gchar *path = NULL;

      path = g_strdup_printf ("/frome/xml-parser/customer/fail/%s", customer_fail_tests[i].name);
      g_test_add_data_func (path, customer_fail_tests[i].xml, test_customer_fail);
    }

  for (i = 0; i < G_N_ELEMENTS (status_success_tests); i++)
    {
      g_autofree gchar *path = NULL;

      path = g_strdup_printf ("/frome/xml-parser/status/success/%s", status_success_tests[i].name);
      g_test_add_data_func (path, &status_success_tests[i], test_status_success);
    }

  for (i = 0; i < G_N_ELEMENTS (status_fail_tests); i++)
    {
      g_autofree gchar *path = NULL;

      path = g_strdup_printf ("/frome/xml-parser/status/fail/%s", status_fail_tests[i].name);
      g_test_add_data_func (path, status_fail_tests[i].xml, test_status_fail);
    }

  return g_test_run ();
}
