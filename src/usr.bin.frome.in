# vim:syntax=apparmor
# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <tunables/global>

# (attach_disconnected) is needed to resolve paths from within the mount
# namespace caused by the ProtectSystem (and other) .service keys.
@bindir@/frome (attach_disconnected) {
  #include <abstractions/chaiwala-base>
  #include <abstractions/nameservice>
  # TODO: sort out D-Bus policy (T3286)
  #include <abstractions/dbus-session>
  #include <abstractions/dbus>

  # Well-known name
  dbus send
        bus=session
        path=/org/freedesktop/DBus
        interface=org.freedesktop.DBus
        member={RequestName,ReleaseName}
        peer=(name=org.freedesktop.DBus),
  dbus bind bus=session name="org.apertis.Frome",
  dbus bind bus=session name="Frome.AccountMgr",
  dbus bind bus=session name="Frome.DnldMgr",

  @bindir@/frome mr,
  @{HOME}/.local/share/** rwk,

  # Needed for tests
  /tmp/** rwk,

  # Getting credentials from the daemon for authorisation checks on its AppArmor label
  dbus send
       bus=session
       path=/org/freedesktop/DBus
       interface=org.freedesktop.DBus
       member="GetConnectionCredentials"
       peer=(name=org.freedesktop.DBus),

  # systemd startup notification
  /run/systemd/notify w,

  # Approved clients
  dbus (send, receive) bus=session peer=(label=@installed_testdir@/test-service),
  dbus (send, receive) bus=session peer=(label=@bindir@/frome-tool),
  # Allow mildenhall-settings app to communicate with frome
  dbus (send, receive) bus=session peer=(label=/usr/Applications/org.apertis.Mildenhall.Settings/bin/*),

  # Query the proxy service
  dbus (send, receive) bus=system peer=(name=org.pacrunner),

  # Download bundles using Newport
  dbus (send, receive) bus=session peer=(label=/usr/bin/newport),
  @{HOME}/Downloads/** rw,

  # Read XDG directory settings
  @{HOME}/.config/user-dirs.dirs r,

  # Install bundles using Ribchester
  dbus (send, receive) bus=system peer=(label=/usr/bin/ribchester),
}
