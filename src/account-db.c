/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "account-db.h"

/* Store accounts information to a key/value file, using the account email as group. */

#define KEY_FIRST_NAME "first-name"
#define KEY_LAST_NAME "last-name"
#define KEY_CUSTOMER_ID "customer-id"
#define KEY_ACCOUNT_TYPE "account-type"
#define KEY_ACTIVE_USER "active-user"

struct _FromeAccountDB
{
  GObject parent;

  gchar *path;
};

typedef enum {
  PROP_PATH = 1,
  /*< private >*/
  PROP_LAST = PROP_PATH
} FromeAccountDBProperty;

static GParamSpec *properties[PROP_LAST + 1];

G_DEFINE_TYPE (FromeAccountDB, frome_account_db, G_TYPE_OBJECT);

static void
frome_account_db_get_property (GObject *object,
                               guint prop_id,
                               GValue *value,
                               GParamSpec *pspec)
{
  FromeAccountDB *self = FROME_ACCOUNT_DB (object);

  switch ((FromeAccountDBProperty) prop_id)
    {
    case PROP_PATH:
      g_value_set_string (value, self->path);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
frome_account_db_set_property (GObject *object,
                               guint prop_id,
                               const GValue *value,
                               GParamSpec *pspec)
{
  FromeAccountDB *self = FROME_ACCOUNT_DB (object);

  switch ((FromeAccountDBProperty) prop_id)
    {
    case PROP_PATH:
      g_clear_pointer (&self->path, g_free);
      self->path = g_value_dup_string (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
frome_account_db_dispose (GObject *object)
{
  FromeAccountDB *self = (FromeAccountDB *) object;

  g_clear_pointer (&self->path, g_free);

  G_OBJECT_CLASS (frome_account_db_parent_class)
      ->dispose (object);
}

static void
frome_account_db_class_init (FromeAccountDBClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->get_property = frome_account_db_get_property;
  object_class->set_property = frome_account_db_set_property;
  object_class->dispose = frome_account_db_dispose;

  /**
   * FromeAccountDB:path:
   *
   * Path of the file storing the database.
   */
  properties[PROP_PATH] = g_param_spec_string (
      "path", "Path", "Path", NULL,
      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (properties), properties);
}

static void
frome_account_db_init (FromeAccountDB *self)
{
}

FromeAccountDB *
frome_account_db_new (const gchar *path)
{
  g_return_val_if_fail (path != NULL, NULL);

  return g_object_new (FROME_TYPE_ACCOUNT_DB,
                       "path", path,
                       NULL);
}

static GKeyFile *
load_db (FromeAccountDB *self,
         gboolean should_exist,
         GError **error)
{
  g_autoptr (GKeyFile) kf = NULL;
  g_autoptr (GError) e = NULL;

  kf = g_key_file_new ();

  if (!g_key_file_load_from_file (kf, self->path, G_KEY_FILE_NONE, &e))
    {
      /* Ignore 'file not found' error if the DB may not exist yet */
      if (should_exist ||
          !g_error_matches (e, G_FILE_ERROR, G_FILE_ERROR_NOENT))
        {
          g_propagate_error (error, g_steal_pointer (&e));
          return NULL;
        }
    }

  return g_steal_pointer (&kf);
}

static gboolean
save_db (FromeAccountDB *self,
         GKeyFile *kf,
         GError **error)
{
  return g_key_file_save_to_file (kf, self->path, error);
}

static const gchar *
account_type_to_str (AccountType type)
{
  switch (type)
    {
    case ACCOUNT_TYPE_DEVEL:
      return "devel";
    case ACCOUNT_TYPE_RELEASE:
      return "release";
    case ACCOUNT_TYPE_BOTH:
      return "both";
    default:
      break;
    }

  g_return_val_if_reached (NULL);
}

static gboolean
account_type_from_str (const gchar *account_type,
                       AccountType *type)
{
  gboolean found = TRUE;

  if (g_strcmp0 (account_type, "devel") == 0)
    *type = ACCOUNT_TYPE_DEVEL;
  else if (g_strcmp0 (account_type, "release") == 0)
    *type = ACCOUNT_TYPE_RELEASE;
  else if (g_strcmp0 (account_type, "both") == 0)
    *type = ACCOUNT_TYPE_BOTH;
  else
    found = FALSE;

  return found;
}

static FromeAccount *
account_new (const gchar *email,
             const gchar *first_name,
             const gchar *last_name,
             const gchar *customer_id,
             AccountType type,
             gboolean active_user)
{
  FromeAccount *account = g_new0 (FromeAccount, 1);

  account->ref_count = 1;
  account->email = g_strdup (email);
  account->first_name = g_strdup (first_name);
  account->last_name = g_strdup (last_name);
  account->customer_id = g_strdup (customer_id);
  account->type = type;
  account->active_user = active_user;
  return account;
}

void
frome_account_unref (FromeAccount *account)
{
  account->ref_count--;

  if (account->ref_count > 0)
    return;

  g_free (account->email);
  g_free (account->first_name);
  g_free (account->last_name);
  g_free (account->customer_id);
  g_free (account);
}

static FromeAccount *
frome_account_ref (FromeAccount *account)
{
  account->ref_count++;

  return account;
}

static FromeAccount *
get_account (GKeyFile *kf,
             const gchar *email)
{
  g_autofree gchar *first_name = NULL, *last_name = NULL, *customer_id = NULL, *account_type = NULL;
  AccountType type;
  gboolean active_user;
  g_autoptr (GError) error = NULL;

  g_return_val_if_fail (email != NULL, NULL);

  first_name = g_key_file_get_string (kf, email, KEY_FIRST_NAME, NULL);
  if (first_name == NULL)
    return NULL;

  last_name = g_key_file_get_string (kf, email, KEY_LAST_NAME, NULL);
  if (last_name == NULL)
    return NULL;

  customer_id = g_key_file_get_string (kf, email, KEY_CUSTOMER_ID, NULL);
  if (customer_id == NULL)
    return NULL;

  account_type = g_key_file_get_string (kf, email, KEY_ACCOUNT_TYPE, NULL);
  if (account_type == NULL)
    return NULL;
  if (!account_type_from_str (account_type, &type))
    return NULL;

  active_user = g_key_file_get_boolean (kf, email, KEY_ACTIVE_USER, &error);
  if (error != NULL)
    return NULL;

  return account_new (email, first_name, last_name, customer_id, type,
                      active_user);
}

gboolean
frome_account_db_add_account (FromeAccountDB *self,
                              const gchar *email,
                              const gchar *first_name,
                              const gchar *last_name,
                              const gchar *customer_id,
                              AccountType account_type,
                              gboolean active_user,
                              GError **error)
{
  g_autoptr (GKeyFile) kf = NULL;
  g_autoptr (FromeAccount) account = NULL;

  g_return_val_if_fail (FROME_IS_ACCOUNT_DB (self), FALSE);
  g_return_val_if_fail (email != NULL, FALSE);
  g_return_val_if_fail (first_name != NULL, FALSE);
  g_return_val_if_fail (last_name != NULL, FALSE);
  g_return_val_if_fail (customer_id != NULL, FALSE);

  kf = load_db (self, FALSE, error);
  if (kf == NULL)
    return FALSE;

  account = get_account (kf, email);
  if (account != NULL)
    {
      g_set_error (error, G_KEY_FILE_ERROR, G_KEY_FILE_ERROR_INVALID_VALUE,
                   "Account '%s' is already present", email);
      return FALSE;
    }

  g_key_file_set_string (kf, email, KEY_FIRST_NAME, first_name);
  g_key_file_set_string (kf, email, KEY_LAST_NAME, last_name);
  g_key_file_set_string (kf, email, KEY_CUSTOMER_ID, customer_id);
  g_key_file_set_string (kf, email, KEY_ACCOUNT_TYPE, account_type_to_str (account_type));
  g_key_file_set_boolean (kf, email, KEY_ACTIVE_USER, active_user);

  return save_db (self, kf, error);
}

/* Returns: (transfer container): a #GPtrArray of FromeAccount */
GPtrArray *
frome_account_db_get_accounts (FromeAccountDB *self)
{
  g_autoptr (GPtrArray) accounts = NULL;
  g_autoptr (GKeyFile) kf = NULL;
  g_auto (GStrv) groups = NULL;
  gsize len;
  guint i;

  g_return_val_if_fail (FROME_IS_ACCOUNT_DB (self), NULL);

  kf = load_db (self, TRUE, NULL);
  if (kf == NULL)
    return g_ptr_array_new ();

  groups = g_key_file_get_groups (kf, &len);
  accounts = g_ptr_array_sized_new (len);

  g_ptr_array_set_free_func (accounts, (GDestroyNotify) frome_account_unref);

  for (i = 0; i < len; i++)
    {
      g_autoptr (FromeAccount) account = get_account (kf, groups[i]);

      if (account == NULL)
        continue;

      g_ptr_array_add (accounts, g_steal_pointer (&account));
    }

  return g_steal_pointer (&accounts);
}

gsize
frome_account_db_get_n_accounts (FromeAccountDB *self)
{
  g_autoptr (GPtrArray) accounts = NULL;

  g_return_val_if_fail (FROME_IS_ACCOUNT_DB (self), 0);

  accounts = frome_account_db_get_accounts (self);
  if (accounts == NULL)
    return 0;

  return accounts->len;
}

/* Returns: (transfer full) */
FromeAccount *
frome_account_db_get_account (FromeAccountDB *self,
                              const gchar *email)
{
  g_autoptr (GKeyFile) kf = NULL;

  g_return_val_if_fail (FROME_IS_ACCOUNT_DB (self), NULL);
  g_return_val_if_fail (email != NULL, NULL);

  kf = load_db (self, TRUE, NULL);
  if (kf == NULL)
    return NULL;

  return get_account (kf, email);
}

gboolean
frome_account_db_remove_account (FromeAccountDB *self,
                                 const gchar *email,
                                 GError **error)
{
  g_autoptr (GKeyFile) kf = NULL;

  g_return_val_if_fail (FROME_IS_ACCOUNT_DB (self), FALSE);
  g_return_val_if_fail (email != NULL, FALSE);

  kf = load_db (self, TRUE, error);
  if (kf == NULL)
    return FALSE;

  if (!g_key_file_remove_group (kf, email, error))
    return FALSE;

  return save_db (self, kf, error);
}

gboolean
frome_account_db_update_active_user (FromeAccountDB *self,
                                     const gchar *email,
                                     gboolean active_user,
                                     GError **error)
{
  g_autoptr (GKeyFile) kf = NULL;

  g_return_val_if_fail (FROME_IS_ACCOUNT_DB (self), FALSE);
  g_return_val_if_fail (email != NULL, FALSE);

  kf = load_db (self, TRUE, error);
  if (kf == NULL)
    return FALSE;

  if (!g_key_file_has_group (kf, email))
    {
      g_set_error (error, G_KEY_FILE_ERROR, G_KEY_FILE_ERROR_GROUP_NOT_FOUND,
                   "No account with email %s", email);
      return FALSE;
    }

  g_key_file_set_boolean (kf, email, KEY_ACTIVE_USER, active_user);

  return save_db (self, kf, error);
}

gboolean
frome_account_db_update_account_type (FromeAccountDB *self,
                                      const gchar *email,
                                      AccountType type,
                                      GError **error)
{
  g_autoptr (GKeyFile) kf = NULL;

  g_return_val_if_fail (FROME_IS_ACCOUNT_DB (self), FALSE);
  g_return_val_if_fail (email != NULL, FALSE);

  kf = load_db (self, TRUE, error);
  if (kf == NULL)
    return FALSE;

  if (!g_key_file_has_group (kf, email))
    {
      g_set_error (error, G_KEY_FILE_ERROR, G_KEY_FILE_ERROR_GROUP_NOT_FOUND,
                   "No account with email %s", email);
      return FALSE;
    }

  g_key_file_set_string (kf, email, KEY_ACCOUNT_TYPE, account_type_to_str (type));

  return save_db (self, kf, error);
}

FromeAccount *
frome_account_db_get_active_user (FromeAccountDB *self)
{
  g_autoptr (GPtrArray) accounts = NULL;
  guint i;

  g_return_val_if_fail (FROME_IS_ACCOUNT_DB (self), NULL);

  accounts = frome_account_db_get_accounts (self);
  if (accounts == NULL)
    return NULL;

  for (i = 0; i < accounts->len; i++)
    {
      FromeAccount *account = g_ptr_array_index (accounts, i);

      if (account->active_user)
        return frome_account_ref (account);
    }

  return NULL;
}
