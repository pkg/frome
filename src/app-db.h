/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __FROME_APP_DB_H__
#define __FROME_APP_DB_H__

#include <gio/gio.h>

#include "app-db-entry.h"

G_BEGIN_DECLS

#define FROME_TYPE_APP_DB (frome_app_db_get_type ())
G_DECLARE_FINAL_TYPE (FromeAppDB, frome_app_db, FROME, APP_DB, GObject)

FromeAppDB *frome_app_db_new (const gchar *path);

FromeAppDBEntry *frome_app_db_find_entry (FromeAppDB *self,
                                          const gchar *product_id,
                                          GError **error);

FromeAppDBEntry *frome_app_db_ensure_entry (FromeAppDB *self,
                                            const gchar *product_id,
                                            GError **error);

GPtrArray *frome_app_db_get_all_entries (FromeAppDB *self,
                                         GError **error);

G_END_DECLS

#endif /* __FROME_APP_DB_H__ */
