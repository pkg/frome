/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __FROME_APP_DB_ENTRY_H__
#define __FROME_APP_DB_ENTRY_H__

#include <gom/gom.h>

#include "constants.h"

G_BEGIN_DECLS

#define FROME_TYPE_APP_DB_ENTRY (frome_app_db_entry_get_type ())
G_DECLARE_FINAL_TYPE (FromeAppDBEntry, frome_app_db_entry, FROME, APP_DB_ENTRY, GomResource)

/* The name of the properties have to match the column names
 * of the old DB implementation as that's what libgom will used for the mapping */
#define APP_DB_ENTRY_PROP_PRODUCT_ID "productID"
#define APP_DB_ENTRY_PROP_DOWNLOAD_URL "DnldUrl"
#define APP_DB_ENTRY_PROP_INSTALL_PATH "InstallPath"
#define APP_DB_ENTRY_PROP_APP_FOLDER_NAME "AppFolderName"
#define APP_DB_ENTRY_PROP_APP_NAME "AppName"
#define APP_DB_ENTRY_PROP_TARGET_APP_STATE "TargetAppState"
#define APP_DB_ENTRY_PROP_STORE_APP_STATE "storeAppState"
#define APP_DB_ENTRY_PROP_APP_STATUS "AppStatus"
#define APP_DB_ENTRY_PROP_APP_SCHEMAS "AppSchemas"
#define APP_DB_ENTRY_PROP_CONFIG_FILE "ConfigFile"
#define APP_DB_ENTRY_PROP_VERSION "Version"
#define APP_DB_ENTRY_PROP_APP_USERS "AppUsers"
#define APP_DB_ENTRY_PROP_ROLLBACK_VERSION "RollbackVersion"
#define APP_DB_ENTRY_PROP_PROCESS_TYPE "ProcessType"
#define APP_DB_ENTRY_PROP_UPDATE_PROD_ID "UpdateProdID"

gboolean frome_app_db_entry_save (FromeAppDBEntry *self,
                                  GError **error);

#define DECLARE_STR_GETTER(prop) \
  const gchar *frome_app_db_entry_get_##prop (FromeAppDBEntry *self);
#define DECLARE_STR_SETTER(prop) \
  void frome_app_db_entry_set_##prop (FromeAppDBEntry *self, const gchar *prop);

DECLARE_STR_GETTER (product_id)

DECLARE_STR_GETTER (download_url)
DECLARE_STR_SETTER (download_url)

DECLARE_STR_GETTER (install_path)
DECLARE_STR_SETTER (install_path)

DECLARE_STR_GETTER (app_folder_name)
DECLARE_STR_SETTER (app_folder_name)

DECLARE_STR_GETTER (app_name)
DECLARE_STR_SETTER (app_name)

AppState frome_app_db_entry_get_target_app_state (FromeAppDBEntry *self);
void frome_app_db_entry_set_target_app_state (FromeAppDBEntry *self,
                                              AppState state);

AppState frome_app_db_entry_get_store_app_state (FromeAppDBEntry *self);
void frome_app_db_entry_set_store_app_state (FromeAppDBEntry *self,
                                             AppState state);

AppStatus frome_app_db_entry_get_app_status (FromeAppDBEntry *self);
void frome_app_db_entry_set_app_status (FromeAppDBEntry *self,
                                        AppStatus status);

DECLARE_STR_GETTER (app_schemas)
DECLARE_STR_SETTER (app_schemas)

DECLARE_STR_GETTER (config_file)
DECLARE_STR_SETTER (config_file)

DECLARE_STR_GETTER (version)
DECLARE_STR_SETTER (version)

DECLARE_STR_GETTER (app_users)
DECLARE_STR_SETTER (app_users)

DECLARE_STR_GETTER (rollback_version)
DECLARE_STR_SETTER (rollback_version)

ProcessType frome_app_db_entry_get_process_type (FromeAppDBEntry *self);
void frome_app_db_entry_set_process_type (FromeAppDBEntry *self,
                                          ProcessType type);

DECLARE_STR_GETTER (update_prod_id)
DECLARE_STR_SETTER (update_prod_id)

G_END_DECLS

#endif /* __FROME_APP_DB_ENTRY_H__ */
