/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "xml-parser.h"

/* Customer Parser */

typedef struct
{
  GMarkupParser parser;

  gboolean in_customer;
  gboolean in_customer_id;
  gchar *customer_id;
} CustomerParser;

static CustomerParser *
customer_parser_new (void)
{
  return g_new0 (CustomerParser, 1);
}

static void
customer_parser_free (CustomerParser *self)
{
  g_free (self->customer_id);
  g_free (self);
}

static void
customer_start_element (GMarkupParseContext *context,
                        const gchar *element_name,
                        const gchar **attribute_names,
                        const gchar **attribute_values,
                        gpointer user_data,
                        GError **error)
{
  CustomerParser *self = user_data;

  if (g_strcmp0 (element_name, "customer") == 0)
    {
      if (self->in_customer)
        {
          g_set_error_literal (error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
                               "Two levels of <customer> markups");
          return;
        }

      self->in_customer = TRUE;
    }
  else if (g_strcmp0 (element_name, "customer_id") == 0)
    {
      if (!self->in_customer)
        {
          g_set_error_literal (error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
                               "<customer_id> not in a <customer> markup");
          return;
        }

      self->in_customer_id = TRUE;
    }
}

static void
customer_end_element (GMarkupParseContext *context,
                      const gchar *element_name,
                      gpointer user_data,
                      GError **error)
{
  CustomerParser *self = user_data;

  if (g_strcmp0 (element_name, "customer") == 0)
    self->in_customer = FALSE;
  else if (g_strcmp0 (element_name, "customer_id") == 0)
    self->in_customer_id = FALSE;
}

static void
customer_text (GMarkupParseContext *context,
               const gchar *text,
               gsize text_len,
               gpointer user_data,
               GError **error)
{
  CustomerParser *self = user_data;

  if (text_len == 0)
    return;

  if (!self->in_customer_id)
    return;

  if (self->customer_id != NULL)
    {
      g_set_error_literal (error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
                           "more than one <customer_id> markup");
      return;
    }

  self->customer_id = g_strndup (text, text_len);
}

gboolean
frome_xml_parser_parse_customer (const gchar *data,
                                 gssize len,
                                 gchar **customer_id,
                                 GError **error)
{
  CustomerParser *self;
  g_autoptr (GMarkupParseContext) ctx = NULL;

  g_return_val_if_fail (data != NULL, FALSE);
  g_return_val_if_fail (len > 0, FALSE);
  g_return_val_if_fail (customer_id != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  self = customer_parser_new ();

  self->parser.start_element = customer_start_element;
  self->parser.end_element = customer_end_element;
  self->parser.text = customer_text;

  /* transfer ownershop of self */
  ctx = g_markup_parse_context_new (&self->parser, 0, self,
                                    (GDestroyNotify) customer_parser_free);

  if (!g_markup_parse_context_parse (ctx, data, len, error))
    return FALSE;

  if (self->customer_id == NULL)
    {
      g_set_error_literal (error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
                           "no <customer_id> markup");
      return FALSE;
    }

  *customer_id = g_steal_pointer (&self->customer_id);

  return TRUE;
}

/* Status Parser */

typedef struct
{
  GMarkupParser parser;

  gboolean in_status;
  gchar *status;
} StatusParser;

static StatusParser *
status_parser_new (void)
{
  return g_new0 (StatusParser, 1);
}

static void
status_parser_free (StatusParser *self)
{
  g_free (self->status);
  g_free (self);
}

static void
status_start_element (GMarkupParseContext *context,
                      const gchar *element_name,
                      const gchar **attribute_names,
                      const gchar **attribute_values,
                      gpointer user_data,
                      GError **error)
{
  StatusParser *self = user_data;

  if (g_strcmp0 (element_name, "status") == 0)
    {
      if (self->in_status)
        {
          g_set_error_literal (error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
                               "Two levels of <status> markups");
          return;
        }

      self->in_status = TRUE;
    }
}

static void
status_end_element (GMarkupParseContext *context,
                    const gchar *element_name,
                    gpointer user_data,
                    GError **error)
{
  StatusParser *self = user_data;

  if (g_strcmp0 (element_name, "status") == 0)
    self->in_status = FALSE;
}

static void
status_text (GMarkupParseContext *context,
             const gchar *text,
             gsize text_len,
             gpointer user_data,
             GError **error)
{
  StatusParser *self = user_data;

  if (text_len == 0)
    return;

  if (!self->in_status)
    return;

  if (self->status != NULL)
    {
      g_set_error_literal (error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
                           "more than one <status> markup");
      return;
    }

  self->status = g_strndup (text, text_len);
}

static gboolean
app_state_from_str (const gchar *str,
                    AppState *status)
{
  if (g_strcmp0 (str, "1") == 0)
    *status = APP_STATE_INITIAL;
  else if (g_strcmp0 (str, "2") == 0)
    *status = APP_STATE_PURCHASED;
  else if (g_strcmp0 (str, "3") == 0)
    *status = APP_STATE_DOWNLOAD_READY;
  else if (g_strcmp0 (str, "4") == 0)
    *status = APP_STATE_DOWNLOADING;
  else if (g_strcmp0 (str, "5") == 0)
    *status = APP_STATE_INSTALLING;
  else if (g_strcmp0 (str, "6") == 0)
    *status = APP_STATE_INSTALLED;
  else if (g_strcmp0 (str, "7") == 0)
    *status = APP_STATE_UPDATE;
  else if (g_strcmp0 (str, "8") == 0)
    *status = APP_STATE_UNINSTALLED;
  else
    return FALSE;

  return TRUE;
}

gboolean
frome_xml_parser_parse_status (const gchar *data,
                               gssize len,
                               AppState *status,
                               GError **error)
{
  StatusParser *self;
  g_autoptr (GMarkupParseContext) ctx = NULL;

  g_return_val_if_fail (data != NULL, FALSE);
  g_return_val_if_fail (len > 0, FALSE);
  g_return_val_if_fail (status != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  self = status_parser_new ();

  self->parser.start_element = status_start_element;
  self->parser.end_element = status_end_element;
  self->parser.text = status_text;

  /* transfer ownershop of self */
  ctx = g_markup_parse_context_new (&self->parser, 0, self,
                                    (GDestroyNotify) status_parser_free);

  if (!g_markup_parse_context_parse (ctx, data, len, error))
    return FALSE;

  if (self->status == NULL)
    {
      g_set_error_literal (error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
                           "no <status> markup");
      return FALSE;
    }

  if (!app_state_from_str (self->status, status))
    {
      g_set_error (error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
                   "Invalid status: %s", self->status);
      return FALSE;
    }

  return TRUE;
}
