/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "service.h"

#include <gio/gio.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <systemd/sd-daemon.h>
#include <unistd.h>

#include "account-db.h"
#include "account-manager.h"
#include "constants.h"
#include "download-manager.h"
#include "web-store-client.h"

#include "dbus/org.apertis.Frome.h"

/* FIXME: The device ID should be generated as follow:
 * a) Fetch the actual device serial number
 * b) Encrypt it using the server public key
 * c) Base64 the result
 *
 * We don't have any way to do a) at the moment so just hardcode a ready to use base64 for testing.
 */
#define DEVICE_ID "gG4BoVkLwZRDeH77BsaqmeHEnwymdAuaNA8R9Fu5KcD8S0q6RB1dReOseijO-68jrbupseuktRyIlknSJYOr2ymn1DCqCVqzhlEnR6xMT3Us9vsr1L7GZmIUEN2aeQ25qtzqz6p3i1Zd0ub8cMuMfYsIBxQXPm3HSRrj4SLfPQ-UdCAfZaXurv2jSO9yybl3i2qwCEP1vKWTLJXQM-HSdKpmMQUWd1FNqB8F_P2SgbR1_Wb6F4txybzN2VIml0Ioa_VBLkYoXnd6SQpsMKhQ2qtAYLSTIX9jfWG4uRG8bbQh-ZgxlyiDZ_MYZJGKz4rBIIPI0TjEgSSAhgJl7qX6zw,,"

struct _FromeService
{
  GObject parent;

  GDBusConnection *conn;     /* owned */
  GDBusConnection *sys_conn; /* owned */

  GCancellable *cancellable;

  GError *run_error;
  gboolean run_exited;
  guint own_name_id;

  FromeWebStoreClient *web_store_client;
  FromeOrgApertisFrome *skeleton;
  FromeAccountManager *account_manager;
  FromeDownloadManager *download_manager;
  FromeAccountDB *account_db;
  FromeAppDB *app_db;
  gchar *default_dir;
};

G_DEFINE_TYPE (FromeService, frome_service, G_TYPE_OBJECT)

static void
frome_service_dispose (GObject *object)
{
  FromeService *self = (FromeService *) object;

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);

  if (self->own_name_id != 0)
    {
      g_bus_unown_name (self->own_name_id);
      self->own_name_id = 0;
    }

  g_clear_object (&self->web_store_client);
  g_clear_object (&self->skeleton);
  g_clear_object (&self->conn);
  g_clear_object (&self->sys_conn);
  g_clear_object (&self->account_manager);
  g_clear_object (&self->download_manager);
  g_clear_object (&self->account_db);
  g_clear_object (&self->app_db);
  g_clear_pointer (&self->default_dir, g_free);

  G_OBJECT_CLASS (frome_service_parent_class)
      ->dispose (object);
}

static void
frome_service_class_init (FromeServiceClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->dispose = frome_service_dispose;
}

static gchar *
dup_default_account_db_path (FromeService *self)
{
  return g_build_filename (self->default_dir, "accounts.ini", NULL);
}

static gboolean
account_db_path_transform_from_func (GBinding *binding,
                                     const GValue *from_value,
                                     GValue *to_value,
                                     gpointer user_data)
{
  FromeService *self = user_data;
  const gchar *path;

  path = g_value_get_string (from_value);
  if (strlen (path) > 0)
    g_value_set_string (to_value, path);
  else
    g_value_take_string (to_value, dup_default_account_db_path (self));

  return TRUE;
}

static gchar *
dup_default_app_db_path (FromeService *self)
{
  return g_build_filename (self->default_dir, "applications.db", NULL);
}

static gboolean
app_db_path_transform_from_func (GBinding *binding,
                                 const GValue *from_value,
                                 GValue *to_value,
                                 gpointer user_data)
{
  FromeService *self = user_data;
  const gchar *path;

  path = g_value_get_string (from_value);
  if (strlen (path) > 0)
    g_value_set_string (to_value, path);
  else
    g_value_take_string (to_value, dup_default_app_db_path (self));

  return TRUE;
}

static void
frome_service_init (FromeService *self)
{
  g_autofree gchar *path = NULL;

  self->default_dir = g_build_filename (g_get_user_data_dir (), "frome", NULL);
  g_mkdir_with_parents (self->default_dir, 0700);

  self->web_store_client = frome_web_store_client_new ();

  path = dup_default_account_db_path (self);
  self->account_db = frome_account_db_new (path);
  g_clear_pointer (&path, g_free);

  path = dup_default_app_db_path (self);
  self->app_db = frome_app_db_new (path);

  self->skeleton = frome_org_apertis_frome_skeleton_new ();

  g_object_bind_property (self->web_store_client, "server-url",
                          self->skeleton, "web-store-url",
                          G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);

  g_object_bind_property_full (self->account_db, "path",
                               self->skeleton, "account-dbpath",
                               G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                               NULL, account_db_path_transform_from_func, self, NULL);

  g_object_bind_property_full (self->app_db, "path",
                               self->skeleton, "application-dbpath",
                               G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL,
                               NULL, app_db_path_transform_from_func, self, NULL);

  g_object_set (self->skeleton, "device-id", DEVICE_ID, NULL);

  self->cancellable = g_cancellable_new ();
}

FromeService *
frome_service_new (void)
{
  return g_object_new (FROME_TYPE_SERVICE, NULL);
}

static void
bus_name_acquired_cb (GDBusConnection *conn,
                      const gchar *name,
                      gpointer user_data)
{
  g_debug ("service started");

  /* Notify systemd we’re ready. */
  sd_notify (0, "READY=1");
}

static void
bus_name_lost_cb (GDBusConnection *conn,
                  const gchar *name,
                  gpointer user_data)
{
  FromeService *self = user_data;

  self->run_error = g_error_new (G_DBUS_ERROR, G_DBUS_ERROR_NAME_HAS_NO_OWNER,
                                 "Lost bus name '%s'", name);
}

static void
download_manager_init_cb (GObject *download_manager,
                          GAsyncResult *result,
                          gpointer user_data)
{
  FromeService *self = user_data;

  if (!g_async_initable_init_finish (G_ASYNC_INITABLE (download_manager), result, &self->run_error))
    return;

  self->own_name_id = g_bus_own_name_on_connection (self->conn, FROME_BUS_NAME,
                                                    G_BUS_NAME_OWNER_FLAGS_NONE,
                                                    bus_name_acquired_cb,
                                                    bus_name_lost_cb, self, NULL);
}

static void
account_manager_init_cb (GObject *account_manager,
                         GAsyncResult *result,
                         gpointer user_data)
{
  FromeService *self = user_data;

  if (!g_async_initable_init_finish (G_ASYNC_INITABLE (account_manager), result, &self->run_error))
    return;

  self->download_manager = frome_download_manager_new (self->conn,
                                                       self->sys_conn,
                                                       self->web_store_client,
                                                       self->account_db,
                                                       DEVICE_ID,
                                                       self->app_db);

  g_async_initable_init_async (G_ASYNC_INITABLE (self->download_manager), G_PRIORITY_DEFAULT,
                               self->cancellable, download_manager_init_cb, self);
}

static void
sys_bus_get_cb (GObject *source,
                GAsyncResult *result,
                gpointer user_data)
{
  FromeService *self = user_data;

  self->sys_conn = g_bus_get_finish (result, &self->run_error);
  if (self->sys_conn == NULL)
    return;

  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (self->skeleton),
                                         self->conn, FROME_PATH,
                                         &self->run_error))
    return;

  self->account_manager = frome_account_manager_new (self->conn,
                                                     self->web_store_client, self->account_db,
                                                     DEVICE_ID);

  g_async_initable_init_async (G_ASYNC_INITABLE (self->account_manager), G_PRIORITY_DEFAULT,
                               self->cancellable, account_manager_init_cb, self);
}

static void
bus_get_cb (GObject *source,
            GAsyncResult *result,
            gpointer user_data)
{
  FromeService *self = user_data;

  self->conn = g_bus_get_finish (result, &self->run_error);
  if (self->conn == NULL)
    return;

  /* Need a connection on the system bus as well */
  g_bus_get (G_BUS_TYPE_SYSTEM, self->cancellable, sys_bus_get_cb, self);
}

void
frome_service_run (FromeService *self,
                   GError **error)
{
  /* Ensure we are not running as root — we don’t need those privileges. */
  if (getuid () == 0 || geteuid () == 0)
    {
      g_set_error_literal (error, G_IO_ERROR,
                           G_IO_ERROR_PERMISSION_DENIED,
                           "This daemon must not be run as root.");
      return;
    }

  g_bus_get (G_BUS_TYPE_SESSION, self->cancellable, bus_get_cb, self);

  while (self->run_error == NULL && !self->run_exited)
    g_main_context_iteration (NULL, TRUE);

  /* Notify systemd we’re shutting down. */
  sd_notify (0, "STOPPING=1");

  if (self->run_error != NULL)
    {
      g_debug ("error, stopping service (%s)", self->run_error->message);

      g_propagate_error (error, self->run_error);
      self->run_error = NULL;
      return;
    }
}
