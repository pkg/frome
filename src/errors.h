/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __FROME_ERRORS_H__
#define __FROME_ERRORS_H__

#include <glib.h>

G_BEGIN_DECLS

#define FROME_ACCOUNT_MANAGER_ERROR (frome_account_manager_error_quark ())

enum
{
  FROME_ACCOUNT_MANAGER_ERROR_INVALID_ARGS,
  FROME_ACCOUNT_MANAGER_ERROR_ACCOUNT_NOT_FOUND,
  FROME_ACCOUNT_MANAGER_ERROR_MISSING_DETAILS,
  FROME_ACCOUNT_MANAGER_ERROR_CANNOT_OPEN_DB,
  FROME_ACCOUNT_MANAGER_ERROR_DB_ERROR,
  FROME_ACCOUNT_MANAGER_ERROR_SETTING_DEFAULT_FAILED,
  FROME_ACCOUNT_MANAGER_ERROR_INVALID_ACCOUNT_TYPE,
  FROME_ACCOUNT_MANAGER_ERROR_SERVER_CONNECTION_FAILED,
  FROME_ACCOUNT_MANAGER_ERROR_DEVICE_ID_NOT_FOUND,
  FROME_ACCOUNT_MANAGER_ERROR_TARGET_ID_NOT_FOUND,
  FROME_ACCOUNT_MANAGER_N_ERRORS /*< skip >*/
};

GQuark frome_account_manager_error_quark (void);

#define FROME_DOWNLOAD_MANAGER_ERROR (frome_download_manager_error_quark ())

enum
{
  FROME_DOWNLOAD_MANAGER_ERROR_INVALID_DOWNLOAD_LINK,
  FROME_DOWNLOAD_MANAGER_ERROR_NO_ACTIVE_ACCOUNT,
  FROME_DOWNLOAD_MANAGER_ERROR_PENDING,
  FROME_DOWNLOAD_MANAGER_ERROR_DB,
  FROME_DOWNLOAD_MANAGER_ERROR_INVALID_ARGS,
  FROME_DOWNLOAD_MANAGER_N_ERRORS /*< skip >*/
};

GQuark frome_download_manager_error_quark (void);

G_END_DECLS

#endif /* __FROME_ERRORS_H__ */
