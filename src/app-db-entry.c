/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "app-db-entry.h"

struct _FromeAppDBEntry
{
  GomResource parent;

  /* Properties, all mapped into the SQL DB */
  gchar *product_id;
  gchar *download_url;
  gchar *install_path;
  gchar *app_folder_name;
  gchar *app_name;
  gchar *target_app_state;
  gchar *store_app_state;
  gchar *app_status;
  gchar *app_schemas;
  gchar *config_file;
  gchar *version;
  gchar *app_users;
  gchar *rollback_version;
  gchar *process_type;
  gchar *update_prod_id;
};

typedef enum {
  PROP_PRODUCT_ID = 1,
  PROP_DOWNLOAD_URL,
  PROP_INSTALL_PATH,
  PROP_APP_FOLDER_NAME,
  PROP_APP_NAME,
  PROP_TARGET_APP_STATE,
  PROP_STORE_APP_STATE,
  PROP_APP_STATUS,
  PROP_APP_SCHEMAS,
  PROP_CONFIG_FILE,
  PROP_VERSION,
  PROP_APP_USERS,
  PROP_ROLLBACK_VERSION,
  PROP_PROCESS_TYPE,
  PROP_UPDATE_PROD_ID,
  /*< private >*/
  PROP_LAST = PROP_UPDATE_PROD_ID
} FromeAppDBEntryProperty;

typedef struct
{
  const gchar *prop_name;
  const gchar *default_value;
} Prop;

/* Needs to stay in the same order as the enum above */
Prop props[] = {
  { NULL, NULL }, /* FromeAppDBEntryProperty is starting at 1 */
  { APP_DB_ENTRY_PROP_PRODUCT_ID, NULL },
  { APP_DB_ENTRY_PROP_DOWNLOAD_URL, NULL },
  { APP_DB_ENTRY_PROP_INSTALL_PATH, NULL },
  { APP_DB_ENTRY_PROP_APP_FOLDER_NAME, NULL },
  { APP_DB_ENTRY_PROP_APP_NAME, NULL },
  { APP_DB_ENTRY_PROP_TARGET_APP_STATE, "1" }, /* APP_STATE_INITIAL */
  { APP_DB_ENTRY_PROP_STORE_APP_STATE, "1" },  /* APP_STATE_INITIAL */
  { APP_DB_ENTRY_PROP_APP_STATUS, NULL },
  { APP_DB_ENTRY_PROP_APP_SCHEMAS, NULL },
  { APP_DB_ENTRY_PROP_CONFIG_FILE, NULL },
  { APP_DB_ENTRY_PROP_VERSION, NULL },
  { APP_DB_ENTRY_PROP_APP_USERS, NULL },
  { APP_DB_ENTRY_PROP_ROLLBACK_VERSION, NULL },
  { APP_DB_ENTRY_PROP_PROCESS_TYPE, NULL },
  { APP_DB_ENTRY_PROP_UPDATE_PROD_ID, NULL },
};

static GParamSpec *properties[PROP_LAST + 1];

G_DEFINE_TYPE (FromeAppDBEntry, frome_app_db_entry, GOM_TYPE_RESOURCE);

static void
frome_app_db_entry_get_property (GObject *object,
                                 guint prop_id,
                                 GValue *value,
                                 GParamSpec *pspec)
{
  FromeAppDBEntry *self = FROME_APP_DB_ENTRY (object);

  switch ((FromeAppDBEntryProperty) prop_id)
    {
    case PROP_PRODUCT_ID:
      g_value_set_string (value, self->product_id);
      break;
    case PROP_DOWNLOAD_URL:
      g_value_set_string (value, self->download_url);
      break;
    case PROP_INSTALL_PATH:
      g_value_set_string (value, self->install_path);
      break;
    case PROP_APP_FOLDER_NAME:
      g_value_set_string (value, self->app_folder_name);
      break;
    case PROP_APP_NAME:
      g_value_set_string (value, self->app_name);
      break;
    case PROP_TARGET_APP_STATE:
      g_value_set_string (value, self->target_app_state);
      break;
    case PROP_STORE_APP_STATE:
      g_value_set_string (value, self->store_app_state);
      break;
    case PROP_APP_STATUS:
      g_value_set_string (value, self->app_status);
      break;
    case PROP_APP_SCHEMAS:
      g_value_set_string (value, self->app_schemas);
      break;
    case PROP_CONFIG_FILE:
      g_value_set_string (value, self->config_file);
      break;
    case PROP_VERSION:
      g_value_set_string (value, self->version);
      break;
    case PROP_APP_USERS:
      g_value_set_string (value, self->app_users);
      break;
    case PROP_ROLLBACK_VERSION:
      g_value_set_string (value, self->rollback_version);
      break;
    case PROP_PROCESS_TYPE:
      g_value_set_string (value, self->process_type);
      break;
    case PROP_UPDATE_PROD_ID:
      g_value_set_string (value, self->update_prod_id);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
frome_app_db_entry_set_property (GObject *object,
                                 guint prop_id,
                                 const GValue *value,
                                 GParamSpec *pspec)
{
  FromeAppDBEntry *self = FROME_APP_DB_ENTRY (object);

  switch ((FromeAppDBEntryProperty) prop_id)
    {
    case PROP_PRODUCT_ID:
      g_clear_pointer (&self->product_id, g_free);
      self->product_id = g_value_dup_string (value);
      break;
    case PROP_DOWNLOAD_URL:
      g_clear_pointer (&self->download_url, g_free);
      self->download_url = g_value_dup_string (value);
      break;
    case PROP_INSTALL_PATH:
      g_clear_pointer (&self->install_path, g_free);
      self->install_path = g_value_dup_string (value);
      break;
    case PROP_APP_FOLDER_NAME:
      g_clear_pointer (&self->app_folder_name, g_free);
      self->app_folder_name = g_value_dup_string (value);
      break;
    case PROP_APP_NAME:
      g_clear_pointer (&self->app_name, g_free);
      self->app_name = g_value_dup_string (value);
      break;
    case PROP_TARGET_APP_STATE:
      g_clear_pointer (&self->target_app_state, g_free);
      self->target_app_state = g_value_dup_string (value);
      break;
    case PROP_STORE_APP_STATE:
      g_clear_pointer (&self->store_app_state, g_free);
      self->store_app_state = g_value_dup_string (value);
      break;
    case PROP_APP_STATUS:
      g_clear_pointer (&self->app_status, g_free);
      self->app_status = g_value_dup_string (value);
      break;
    case PROP_APP_SCHEMAS:
      g_clear_pointer (&self->app_schemas, g_free);
      self->app_schemas = g_value_dup_string (value);
      break;
    case PROP_CONFIG_FILE:
      g_clear_pointer (&self->config_file, g_free);
      self->config_file = g_value_dup_string (value);
      break;
    case PROP_VERSION:
      g_clear_pointer (&self->version, g_free);
      self->version = g_value_dup_string (value);
      break;
    case PROP_APP_USERS:
      g_clear_pointer (&self->app_users, g_free);
      self->app_users = g_value_dup_string (value);
      break;
    case PROP_ROLLBACK_VERSION:
      g_clear_pointer (&self->rollback_version, g_free);
      self->rollback_version = g_value_dup_string (value);
      break;
    case PROP_PROCESS_TYPE:
      g_clear_pointer (&self->process_type, g_free);
      self->process_type = g_value_dup_string (value);
      break;
    case PROP_UPDATE_PROD_ID:
      g_clear_pointer (&self->update_prod_id, g_free);
      self->update_prod_id = g_value_dup_string (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
frome_app_db_entry_dispose (GObject *object)
{
  FromeAppDBEntry *self = (FromeAppDBEntry *) object;

  g_clear_pointer (&self->product_id, g_free);
  g_clear_pointer (&self->download_url, g_free);
  g_clear_pointer (&self->install_path, g_free);
  g_clear_pointer (&self->app_folder_name, g_free);
  g_clear_pointer (&self->app_name, g_free);
  g_clear_pointer (&self->target_app_state, g_free);
  g_clear_pointer (&self->store_app_state, g_free);
  g_clear_pointer (&self->app_status, g_free);
  g_clear_pointer (&self->app_schemas, g_free);
  g_clear_pointer (&self->config_file, g_free);
  g_clear_pointer (&self->version, g_free);
  g_clear_pointer (&self->app_users, g_free);
  g_clear_pointer (&self->rollback_version, g_free);
  g_clear_pointer (&self->process_type, g_free);
  g_clear_pointer (&self->update_prod_id, g_free);

  G_OBJECT_CLASS (frome_app_db_entry_parent_class)
      ->dispose (object);
}

static void
frome_app_db_entry_class_init (FromeAppDBEntryClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  GomResourceClass *resource_class;
  guint i;

  object_class->get_property = frome_app_db_entry_get_property;
  object_class->set_property = frome_app_db_entry_set_property;
  object_class->dispose = frome_app_db_entry_dispose;

  resource_class = GOM_RESOURCE_CLASS (klass);
  gom_resource_class_set_table (resource_class, "service");

  for (i = 1; i <= PROP_LAST; i++)
    properties[i] = g_param_spec_string (
        props[i].prop_name, props[i].prop_name, props[i].prop_name,
        props[i].default_value,
        G_PARAM_READWRITE | G_PARAM_CONSTRUCT | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (properties), properties);

  gom_resource_class_set_primary_key (resource_class, APP_DB_ENTRY_PROP_PRODUCT_ID);
}

static void
frome_app_db_entry_init (FromeAppDBEntry *self)
{
}

gboolean
frome_app_db_entry_save (FromeAppDBEntry *self,
                         GError **error)
{
  g_return_val_if_fail (FROME_IS_APP_DB_ENTRY (self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return gom_resource_save_sync (GOM_RESOURCE (self), error);
}

#define DEFINE_STR_GETTER(prop)                                      \
  const gchar *frome_app_db_entry_get_##prop (FromeAppDBEntry *self) \
  {                                                                  \
    g_return_val_if_fail (FROME_IS_APP_DB_ENTRY (self), NULL);       \
    return self->prop;                                               \
  }

#define DEFINE_STR_SETTER(prop, PROP)                                           \
  void frome_app_db_entry_set_##prop (FromeAppDBEntry *self, const gchar *prop) \
  {                                                                             \
    g_return_if_fail (FROME_IS_APP_DB_ENTRY (self));                            \
    g_return_if_fail (prop != NULL);                                            \
                                                                                \
    g_clear_pointer (&self->prop, g_free);                                      \
    self->prop = g_strdup (prop);                                               \
    g_object_notify (G_OBJECT (self), APP_DB_ENTRY_PROP_##PROP);                \
  }

DEFINE_STR_GETTER (product_id)

DEFINE_STR_GETTER (download_url)
DEFINE_STR_SETTER (download_url, DOWNLOAD_URL)

DEFINE_STR_GETTER (install_path)
DEFINE_STR_SETTER (install_path, INSTALL_PATH)

DEFINE_STR_GETTER (app_folder_name)
DEFINE_STR_SETTER (app_folder_name, APP_FOLDER_NAME)

DEFINE_STR_GETTER (app_name)
DEFINE_STR_SETTER (app_name, APP_NAME)

typedef struct
{
  AppState state;
  const gchar *str;
} AppStateMapping;

/* Conversion between the enum and its representation in the DB */
AppStateMapping app_state_mapping[] = {
  { APP_STATE_INITIAL, "1" },
  { APP_STATE_PURCHASED, "2" },
  { APP_STATE_DOWNLOAD_READY, "3" },
  { APP_STATE_DOWNLOADING, "4" },
  { APP_STATE_INSTALLING, "5" },
  { APP_STATE_INSTALLED, "6" },
  { APP_STATE_UPDATE, "7" },
  { APP_STATE_UNINSTALLED, "8" },
};

static AppState
app_state_from_db_str (const gchar *str)
{
  guint i;

  for (i = 0; i < G_N_ELEMENTS (app_state_mapping); i++)
    {
      if (g_strcmp0 (app_state_mapping[i].str, str) == 0)
        return app_state_mapping[i].state;
    }

  g_return_val_if_reached (0);
}

static const gchar *
app_state_to_db_str (AppState state)
{
  guint i;

  for (i = 0; i < G_N_ELEMENTS (app_state_mapping); i++)
    {
      if (app_state_mapping[i].state == state)
        return app_state_mapping[i].str;
    }

  g_return_val_if_reached (NULL);
}

AppState
frome_app_db_entry_get_target_app_state (FromeAppDBEntry *self)
{
  g_return_val_if_fail (FROME_IS_APP_DB_ENTRY (self), 0);

  return app_state_from_db_str (self->target_app_state);
}

void
frome_app_db_entry_set_target_app_state (FromeAppDBEntry *self,
                                         AppState state)
{
  g_return_if_fail (FROME_IS_APP_DB_ENTRY (self));

  g_clear_pointer (&self->target_app_state, g_free);
  self->target_app_state = g_strdup (app_state_to_db_str (state));
  g_object_notify (G_OBJECT (self), APP_DB_ENTRY_PROP_TARGET_APP_STATE);
}

AppState
frome_app_db_entry_get_store_app_state (FromeAppDBEntry *self)
{
  g_return_val_if_fail (FROME_IS_APP_DB_ENTRY (self), 0);

  return app_state_from_db_str (self->store_app_state);
}

void
frome_app_db_entry_set_store_app_state (FromeAppDBEntry *self,
                                        AppState state)
{
  g_return_if_fail (FROME_IS_APP_DB_ENTRY (self));

  g_clear_pointer (&self->store_app_state, g_free);
  self->store_app_state = g_strdup (app_state_to_db_str (state));
  g_object_notify (G_OBJECT (self), APP_DB_ENTRY_PROP_STORE_APP_STATE);
}

typedef struct
{
  AppStatus status;
  const gchar *str;
} AppStatusMapping;

AppStatusMapping app_status_mapping[] = {
  { APP_STATUS_INSTALLATION_SUCCESS, "0" },
  { APP_STATUS_DOWNLOAD_STARTED, "1" },
  { APP_STATUS_DOWNLOADING, "2" },
  { APP_STATUS_DOWNLOAD_COMPLETE, "3" },
  { APP_STATUS_DOWNLOADING_FATAL_ERROR, "4" },
  { APP_STATUS_EXTRACTION_IN_PROGRESS, "5" },
  { APP_STATUS_EXTRACTION_FAILED, "6" },
  { APP_STATUS_RUNNING_INSTALL_SCRIPT, "7" },
  { APP_STATUS_UNINSTALLATION_FAILED, "8" },
  { APP_STATUS_CREATE_TEMP_SUBVOL, "9" },
  { APP_STATUS_RENAME_TEMP_SUBVOL, "10" },
  { APP_STATUS_MOUNT_SUBVOL, "11" },
  { APP_STATUS_UNMOUNT_SUBVOL, "12" },
  { APP_STATUS_UPGRADE_SUBVOL, "13" },
  { APP_STATUS_UNINSTALL_WHILE_UPDATING, "14" }
};

static AppStatus
app_status_from_db_str (const gchar *str)
{
  guint i;

  for (i = 0; i < G_N_ELEMENTS (app_status_mapping); i++)
    {
      if (g_strcmp0 (app_status_mapping[i].str, str) == 0)
        return app_status_mapping[i].status;
    }

  g_return_val_if_reached (0);
}

static const gchar *
app_status_to_db_str (AppStatus status)
{
  guint i;

  for (i = 0; i < G_N_ELEMENTS (app_status_mapping); i++)
    {
      if (app_status_mapping[i].status == status)
        return app_status_mapping[i].str;
    }

  g_return_val_if_reached (NULL);
}

AppStatus
frome_app_db_entry_get_app_status (FromeAppDBEntry *self)
{
  g_return_val_if_fail (FROME_IS_APP_DB_ENTRY (self), 0);

  return app_status_from_db_str (self->app_status);
}

void
frome_app_db_entry_set_app_status (FromeAppDBEntry *self,
                                   AppStatus status)
{
  g_return_if_fail (FROME_IS_APP_DB_ENTRY (self));

  g_clear_pointer (&self->app_status, g_free);
  self->app_status = g_strdup (app_status_to_db_str (status));
  g_object_notify (G_OBJECT (self), APP_DB_ENTRY_PROP_APP_STATUS);
}

DEFINE_STR_GETTER (app_schemas)
DEFINE_STR_SETTER (app_schemas, APP_SCHEMAS)

DEFINE_STR_GETTER (config_file)
DEFINE_STR_SETTER (config_file, CONFIG_FILE)

DEFINE_STR_GETTER (version)
DEFINE_STR_SETTER (version, VERSION)

DEFINE_STR_GETTER (app_users)
DEFINE_STR_SETTER (app_users, APP_USERS)

DEFINE_STR_GETTER (rollback_version)
DEFINE_STR_SETTER (rollback_version, ROLLBACK_VERSION)

typedef struct
{
  ProcessType type;
  const gchar *str;
} ProcessTypeMapping;

ProcessTypeMapping process_type_mapping[] = {
  { PROCESS_TYPE_APP_INSTALL, "1" },
  { PROCESS_TYPE_APP_REINSTALL, "2" },
  { PROCESS_TYPE_APP_UNINSTALL, "3" },
  { PROCESS_TYPE_APP_UPDATE, "4" },
  { PROCESS_TYPE_APP_ROLLBACK, "5" },
  { PROCESS_TYPE_APP_USB_INSTALLATION, "6" },
  { PROCESS_TYPE_APP_INTERNAL_INSTALLATION, "7" },
  { PROCESS_TYPE_APP_REMOVAL, "8" },
};

static ProcessType
process_type_from_db_str (const gchar *str)
{
  guint i;

  for (i = 0; i < G_N_ELEMENTS (process_type_mapping); i++)
    {
      if (g_strcmp0 (process_type_mapping[i].str, str) == 0)
        return process_type_mapping[i].type;
    }

  g_return_val_if_reached (0);
}

static const gchar *
process_type_to_db_str (ProcessType type)
{
  guint i;

  for (i = 0; i < G_N_ELEMENTS (process_type_mapping); i++)
    {
      if (process_type_mapping[i].type == type)
        return process_type_mapping[i].str;
    }

  g_return_val_if_reached (NULL);
}

ProcessType
frome_app_db_entry_get_process_type (FromeAppDBEntry *self)
{
  g_return_val_if_fail (FROME_IS_APP_DB_ENTRY (self), 0);

  return process_type_from_db_str (self->process_type);
}

void
frome_app_db_entry_set_process_type (FromeAppDBEntry *self,
                                     ProcessType type)
{
  g_return_if_fail (FROME_IS_APP_DB_ENTRY (self));

  g_clear_pointer (&self->process_type, g_free);
  self->process_type = g_strdup (process_type_to_db_str (type));
  g_object_notify (G_OBJECT (self), APP_DB_ENTRY_PROP_PROCESS_TYPE);
}

DEFINE_STR_GETTER (update_prod_id)
DEFINE_STR_SETTER (update_prod_id, UPDATE_PROD_ID)
