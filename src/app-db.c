/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "app-db.h"

struct _FromeAppDB
{
  GObject parent;

  gchar *path;

  GomAdapter *adapter;
  GomRepository *repository;
};

typedef enum {
  PROP_PATH = 1,
  /*< private >*/
  PROP_LAST = PROP_PATH
} FromeAppDBProperty;

static GParamSpec *properties[PROP_LAST + 1];

G_DEFINE_TYPE (FromeAppDB, frome_app_db, G_TYPE_OBJECT);

static void
open_db (FromeAppDB *self)
{
  g_autoptr (GError) error = NULL;
  GList *object_types = NULL;

  g_return_if_fail (self->path != NULL);

  if (self->adapter != NULL)
    {
      gom_adapter_close_sync (self->adapter, NULL);
      g_clear_object (&self->adapter);
    }

  g_clear_object (&self->repository);

  self->adapter = gom_adapter_new ();

  if (!gom_adapter_open_sync (self->adapter, self->path, &error))
    {
      g_warning ("Failed to open DB: %s", error->message);
      return;
    }

  self->repository = gom_repository_new (self->adapter);

  object_types = g_list_prepend (object_types,
                                 GUINT_TO_POINTER (FROME_TYPE_APP_DB_ENTRY));

  if (!gom_repository_automatic_migrate_sync (self->repository, 1, object_types,
                                              &error))
    {
      g_warning ("Failed to migrate DB: %s", error->message);
      return;
    }
}

static void
frome_app_db_get_property (GObject *object,
                           guint prop_id,
                           GValue *value,
                           GParamSpec *pspec)
{
  FromeAppDB *self = FROME_APP_DB (object);

  switch ((FromeAppDBProperty) prop_id)
    {
    case PROP_PATH:
      g_value_set_string (value, self->path);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
frome_app_db_set_property (GObject *object,
                           guint prop_id,
                           const GValue *value,
                           GParamSpec *pspec)
{
  FromeAppDB *self = FROME_APP_DB (object);

  switch ((FromeAppDBProperty) prop_id)
    {
    case PROP_PATH:
      g_clear_pointer (&self->path, g_free);
      self->path = g_value_dup_string (value);
      open_db (self);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
frome_app_db_dispose (GObject *object)
{
  FromeAppDB *self = (FromeAppDB *) object;

  if (self->adapter != NULL)
    {
      gom_adapter_close_sync (self->adapter, NULL);
      g_clear_object (&self->adapter);
    }
  g_clear_pointer (&self->path, g_free);

  g_clear_object (&self->repository);

  G_OBJECT_CLASS (frome_app_db_parent_class)
      ->dispose (object);
}

static void
frome_app_db_class_init (FromeAppDBClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->get_property = frome_app_db_get_property;
  object_class->set_property = frome_app_db_set_property;
  object_class->dispose = frome_app_db_dispose;

  /**
   * FromeAppDB:path:
   *
   * Path of the file storing the database.
   */
  properties[PROP_PATH] = g_param_spec_string (
      "path", "Path", "Path", NULL,
      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (properties), properties);
}

static void
frome_app_db_init (FromeAppDB *self)
{
}

FromeAppDB *
frome_app_db_new (const gchar *path)
{
  g_return_val_if_fail (path != NULL, NULL);

  return g_object_new (FROME_TYPE_APP_DB,
                       "path", path,
                       NULL);
}

static FromeAppDBEntry *
create_entry (FromeAppDB *self,
              const gchar *product_id)
{
  g_autoptr (FromeAppDBEntry) entry = NULL;
  g_autoptr (GError) error = NULL;

  entry = g_object_new (FROME_TYPE_APP_DB_ENTRY,
                        "repository", self->repository,
                        APP_DB_ENTRY_PROP_PRODUCT_ID, product_id,
                        NULL);

  if (!frome_app_db_entry_save (entry, &error))
    g_warning ("Failed to save entry %s: %s", product_id, error->message);

  return g_steal_pointer (&entry);
}

FromeAppDBEntry *
frome_app_db_find_entry (FromeAppDB *self,
                         const gchar *product_id,
                         GError **error)
{
  g_autoptr (FromeAppDBEntry) entry = NULL;
  g_autoptr (GomFilter) filter = NULL;
  g_auto (GValue) value = G_VALUE_INIT;
  g_autoptr (GError) err = NULL;

  g_return_val_if_fail (FROME_IS_APP_DB (self), NULL);
  g_return_val_if_fail (product_id != NULL, NULL);

  g_value_init (&value, G_TYPE_STRING);
  g_value_set_string (&value, product_id);
  filter = gom_filter_new_eq (FROME_TYPE_APP_DB_ENTRY,
                              APP_DB_ENTRY_PROP_PRODUCT_ID, &value);

  entry = FROME_APP_DB_ENTRY (gom_repository_find_one_sync (self->repository,
                                                            FROME_TYPE_APP_DB_ENTRY,
                                                            filter, &err));

  if (err != NULL)
    {
      /* don't report as an error if the entry isn't found in the DB */
      if (!g_error_matches (err, GOM_ERROR, GOM_ERROR_REPOSITORY_EMPTY_RESULT))
        g_propagate_error (error, g_steal_pointer (&err));
    }

  return g_steal_pointer (&entry);
}

FromeAppDBEntry *
frome_app_db_ensure_entry (FromeAppDB *self,
                           const gchar *product_id,
                           GError **error)
{
  g_autoptr (FromeAppDBEntry) entry = NULL;
  g_autoptr (GError) err = NULL;

  g_return_val_if_fail (FROME_IS_APP_DB (self), NULL);
  g_return_val_if_fail (product_id != NULL, NULL);

  entry = frome_app_db_find_entry (self, product_id, &err);
  if (err != NULL)
    {
      g_propagate_error (error, g_steal_pointer (&err));
      return NULL;
    }

  if (entry == NULL)
    entry = create_entry (self, product_id);

  return g_steal_pointer (&entry);
}

GPtrArray *
frome_app_db_get_all_entries (FromeAppDB *self,
                              GError **error)
{
  g_autoptr (GomResourceGroup) group = NULL;
  g_autoptr (GPtrArray) result = NULL;
  guint i, n;

  group = gom_repository_find_sync (self->repository, FROME_TYPE_APP_DB_ENTRY,
                                    NULL, error);
  if (group == NULL)
    return NULL;

  n = gom_resource_group_get_count (group);

  if (!gom_resource_group_fetch_sync (group, 0, n, error))
    return NULL;

  result = g_ptr_array_sized_new (n);
  g_ptr_array_set_free_func (result, g_object_unref);

  for (i = 0; i < n; i++)
    {
      GomResource *ressource;

      ressource = gom_resource_group_get_index (group, i);
      if (ressource == NULL)
        {
          g_warning ("Failed to retrieve item %u", i);
          continue;
        }

      g_ptr_array_add (result, g_object_ref (ressource));
    }

  return g_steal_pointer (&result);
}
