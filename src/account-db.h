/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __FROME_ACCOUNT_DB_H__
#define __FROME_ACCOUNT_DB_H__

#include <gio/gio.h>

#include "constants.h"
#include "web-store-client.h"

G_BEGIN_DECLS

#define FROME_TYPE_ACCOUNT_DB (frome_account_db_get_type ())
G_DECLARE_FINAL_TYPE (FromeAccountDB, frome_account_db, FROME, ACCOUNT_DB, GObject)

FromeAccountDB *frome_account_db_new (const gchar *path);

gboolean frome_account_db_add_account (FromeAccountDB *self,
                                       const gchar *email,
                                       const gchar *first_name,
                                       const gchar *last_name,
                                       const gchar *customer_id,
                                       AccountType account_type,
                                       gboolean active_user,
                                       GError **error);

typedef struct
{
  gchar *email;
  gchar *first_name;
  gchar *last_name;
  gchar *customer_id;
  AccountType type;
  gboolean active_user;

  /*< private >*/
  guint ref_count;
} FromeAccount;

void frome_account_unref (FromeAccount *account);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (FromeAccount, frome_account_unref)

GPtrArray *frome_account_db_get_accounts (FromeAccountDB *self);

gsize frome_account_db_get_n_accounts (FromeAccountDB *self);

FromeAccount *frome_account_db_get_account (FromeAccountDB *self,
                                            const gchar *email);

gboolean frome_account_db_remove_account (FromeAccountDB *self,
                                          const gchar *email,
                                          GError **error);

gboolean frome_account_db_update_active_user (FromeAccountDB *self,
                                              const gchar *email,
                                              gboolean active_user,
                                              GError **error);

gboolean frome_account_db_update_account_type (FromeAccountDB *self,
                                               const gchar *email,
                                               AccountType type,
                                               GError **error);

FromeAccount *frome_account_db_get_active_user (FromeAccountDB *self);

G_END_DECLS

#endif /* __FROME_ACCOUNT_DB_H__ */
