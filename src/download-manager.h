/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __FROME_DOWNLOAD_MANAGER_H__
#define __FROME_DOWNLOAD_MANAGER_H__

#include <gio/gio.h>

#include "account-db.h"
#include "app-db.h"
#include "web-store-client.h"

G_BEGIN_DECLS

#define FROME_TYPE_DOWNLOAD_MANAGER (frome_download_manager_get_type ())
G_DECLARE_FINAL_TYPE (FromeDownloadManager, frome_download_manager, FROME, DOWNLOAD_MANAGER, GObject)

FromeDownloadManager *frome_download_manager_new (GDBusConnection *conn,
                                                  GDBusConnection *sys_conn,
                                                  FromeWebStoreClient *web_store_client,
                                                  FromeAccountDB *account_db,
                                                  const gchar *device_id,
                                                  FromeAppDB *app_db);

G_END_DECLS

#endif /* __FROME_DOWNLOAD_MANAGER_H__ */
